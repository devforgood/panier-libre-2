Feature('Users view');

Scenario('can navigate to users', async(I) => {
    I.amOnPage(process.env.DOMAIN);
    I.click('#usersLink')
    I.amOnPage(process.env.DOMAIN + `/users`);
});

Scenario('can navigate to create user', async(I) => {
    I.amOnPage(process.env.DOMAIN);
    I.click('#usersLink')
    I.click('#createUser')
    I.amOnPage(process.env.DOMAIN + `/create-user`);
});