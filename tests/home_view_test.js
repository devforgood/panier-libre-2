Feature('Home view');

Scenario('can see menu', async(I) => {
    I.amOnPage(process.env.DOMAIN);
    I.see('MENU');
});

Scenario('can see users option', async(I) => {
    I.amOnPage(process.env.DOMAIN);
    I.seeElement('#usersLink')
});