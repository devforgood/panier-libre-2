let assert = require('assert')
Feature('Create users');

Scenario('can interact with users endpoint', async(I) => {
    let res = await I.sendGetRequest('/alive')
    console.log(res.data)
});

Scenario('can navigate to create user', async(I) => {
    I.amOnPage(process.env.DOMAIN);
    I.click('#usersLink')
    I.click('#createUser')
    I.amOnPage(process.env.DOMAIN + `/create-user`);
});

Scenario('can create user', async(I) => {
    I.amOnPage(process.env.DOMAIN);
    I.click('#usersLink')
    I.click('#createUser')
    I.fillField('#email', "test@test.com")
    I.fillField('#password', '123456')
    I.click('#save')
    let userId = await I.grabValueFrom("#id")
    assert.notStrictEqual(null, userId)
});